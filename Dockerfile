FROM openjdk:13-jdk-alpine
LABEL maintainer="Pedro Pacheco <pedrovcpacheco@yahoo.com>"
RUN apk add git && \    
    git clone https://gitlab.com/ifpb-es/linkedjobs/linkedjobs.git 
WORKDIR linkedjobs
RUN  chmod +x mvnw 
ENTRYPOINT ["./mvnw"]
CMD ["quarkus:dev"]
EXPOSE 8080
