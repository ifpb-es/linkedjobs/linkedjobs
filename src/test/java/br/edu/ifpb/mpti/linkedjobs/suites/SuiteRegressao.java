package br.edu.ifpb.mpti.linkedjobs.suites;

import java.io.IOException;

import com.linkedjobs.utils.PropertiesAssistent;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import br.edu.ifpb.mpti.linkedjobs.testes.CT10DetalhamentoDaVagasPorID;
import br.edu.ifpb.mpti.linkedjobs.testes.CT11ObterLinkDaVaga;
import br.edu.ifpb.mpti.linkedjobs.testes.CT1ValidarDadosComModeloNoDatabase;
import br.edu.ifpb.mpti.linkedjobs.testes.CT2ValidarDadosInseridosNaAplicaçãoComModelo;
import br.edu.ifpb.mpti.linkedjobs.testes.CT3ConsultaDeVagasListagem;
import br.edu.ifpb.mpti.linkedjobs.testes.CT4ConsultaDeVagasPorTexto;
import br.edu.ifpb.mpti.linkedjobs.testes.CT5ConsultaDeVagasComFiltroDeRemuneração;
import br.edu.ifpb.mpti.linkedjobs.testes.CT6ConsultaDeVagasComFiltroDeLocalização;
import br.edu.ifpb.mpti.linkedjobs.testes.CT7ConsultaDeVagasComPesoDeGeolocalização;
import br.edu.ifpb.mpti.linkedjobs.testes.CT8ConsultaDeVagasComPesoDeSalário;
import br.edu.ifpb.mpti.linkedjobs.testes.CT9ConsultaDeVagasComPesosAplicados;


@RunWith(Suite.class)
@SuiteClasses(
		{
			CT1ValidarDadosComModeloNoDatabase.class,
			CT2ValidarDadosInseridosNaAplicaçãoComModelo.class,
			CT3ConsultaDeVagasListagem.class,
			CT4ConsultaDeVagasPorTexto.class,
			CT5ConsultaDeVagasComFiltroDeRemuneração.class,
			CT6ConsultaDeVagasComFiltroDeLocalização.class,
			CT7ConsultaDeVagasComPesoDeGeolocalização.class,
			CT8ConsultaDeVagasComPesoDeSalário.class,
			CT9ConsultaDeVagasComPesosAplicados.class,
			CT10DetalhamentoDaVagasPorID.class,
			CT11ObterLinkDaVaga.class
			})
public class SuiteRegressao {
	public static PropertiesAssistent propertiesAssistent = new PropertiesAssistent("configuration.properties");
	public static String testPlan="PT - LinkedJobs v1.0 (Regressão)";
	public static String build="Linkedjobs-v1.0_16062020";
	
	@BeforeClass
	public static void setandoParams() {
		try {
			propertiesAssistent.setProperties("TEST_PLAN", testPlan);
			propertiesAssistent.setProperties("BUILD", build);
			
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
}
