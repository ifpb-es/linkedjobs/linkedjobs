package br.edu.ifpb.mpti.linkedjobs.testes;

import static com.jayway.restassured.RestAssured.baseURI;
import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.hasItem;

import java.io.IOException;

import com.linkedjobs.utils.TestLinkConection;

import org.junit.Test;

import br.eti.kinoshita.testlinkjavaapi.util.TestLinkAPIException;

public class CT4ConsultaDeVagasPorTexto extends TestLinkConection{
	
	
	public CT4ConsultaDeVagasPorTexto(){
		try {
			baseURI = propertiesAssistent.getProperties().get("BASE_URI").toString();
			testProject = propertiesAssistent.getProperties().get("TEST_PROJECT").toString();
			testPlan = propertiesAssistent.getProperties().get("TEST_PLAN").toString();
			build = propertiesAssistent.getProperties().get("BUILD").toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testConsultaVagaTexto() throws TestLinkAPIException {
		testCase ="CT4 - Consulta de Vagas - Por texto";
		
		
		try {
			
			given()
		 	.contentType("application/json")
		 .when()
		    .get("/busca?stringDeBusca=string")
		 .then()
		    .statusCode(200)
		    .body("nome", hasItem("string"))
		    .assertThat()
		       .body(matchesJsonSchemaInClasspath("schema_linkedjobs.json"));
			notes="PASSOU - Teste automatizado CT4 - Consulta de Vagas - Texto.(testConsultaVagaTexto)";
			
		} catch (Exception e) {
			notes="FALHOU - Teste automatizado CT4 - Consulta de Vagas - Texto.(testConsultaVagaListagem)";
		}finally{
			reportResult(testProject, testPlan, testCase, build, notes, result);
		}
	}
	

}
