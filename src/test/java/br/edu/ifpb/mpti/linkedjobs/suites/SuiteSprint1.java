package br.edu.ifpb.mpti.linkedjobs.suites;

import java.io.IOException;

import com.linkedjobs.utils.PropertiesAssistent;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import br.edu.ifpb.mpti.linkedjobs.testes.CT1ValidarDadosComModeloNoDatabase;
import br.edu.ifpb.mpti.linkedjobs.testes.CT2ValidarDadosInseridosNaAplicaçãoComModelo;
import br.edu.ifpb.mpti.linkedjobs.testes.CT3ConsultaDeVagasListagem;
import br.edu.ifpb.mpti.linkedjobs.testes.CT4ConsultaDeVagasPorTexto;


@RunWith(Suite.class)
@SuiteClasses(
		{
			CT1ValidarDadosComModeloNoDatabase.class,
			CT2ValidarDadosInseridosNaAplicaçãoComModelo.class,
			CT3ConsultaDeVagasListagem.class,
			CT4ConsultaDeVagasPorTexto.class
			})
public class SuiteSprint1 {
	public static PropertiesAssistent propertiesAssistent = new PropertiesAssistent("configuration.properties");
	public static String testPlan="PT - LinkedJobs v0.01 (Sprint 1)";
	public static String build="Linkedjobs-v0.01_19052020";
	
	@BeforeClass
	public static void setandoParams() {
		try {
			propertiesAssistent.setProperties("TEST_PLAN", testPlan);
			propertiesAssistent.setProperties("BUILD", build);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
}