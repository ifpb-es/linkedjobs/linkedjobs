package br.edu.ifpb.mpti.linkedjobs.testes;

import static com.jayway.restassured.RestAssured.baseURI;
import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.hasItem;

import java.io.IOException;

import com.linkedjobs.utils.TestLinkConection;

import org.junit.Test;

import br.eti.kinoshita.testlinkjavaapi.util.TestLinkAPIException;

public class CT6ConsultaDeVagasComFiltroDeLocalização  extends TestLinkConection{
	
	public CT6ConsultaDeVagasComFiltroDeLocalização(){
		
		try {
			baseURI = propertiesAssistent.getProperties().get("BASE_URI").toString();
			testProject = propertiesAssistent.getProperties().get("TEST_PROJECT").toString();
			testPlan = propertiesAssistent.getProperties().get("TEST_PLAN").toString();
			build = propertiesAssistent.getProperties().get("BUILD").toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testBuscarFiltroLocalizacao() throws TestLinkAPIException {
		testCase="CT6 - Consulta de Vagas - Com Filtro de Localização";
		
		try {
			
			given()
		 	.contentType("application/json")
		 .when()
		    .get("/busca/municipio?municipio=Rio")
		 .then()
		    .statusCode(200)
		    .body("municipio", hasItem("Rio de Janeiro-RJ"))
		    .assertThat()
		       .body(matchesJsonSchemaInClasspath("schema_linkedjobs.json"));
			notes="PASSOU - Teste automatizado CT6 - Consulta de Vagas - Com Filtro de Localização.(testBuscarFiltroLocalizacao)";
			
		} catch (Exception e) {
			notes="FALHOU - Teste automatizado CT6 - Consulta de Vagas - Com Filtro de Localização.(testBuscarFiltroLocalizacao)";
		}finally{
			reportResult(testProject, testPlan, testCase, build, notes, result);
		}
	}
}
