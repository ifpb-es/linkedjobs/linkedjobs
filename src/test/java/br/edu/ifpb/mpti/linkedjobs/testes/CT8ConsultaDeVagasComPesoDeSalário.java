package br.edu.ifpb.mpti.linkedjobs.testes;

import static com.jayway.restassured.RestAssured.baseURI;
import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.hasItem;

import java.io.IOException;

import com.linkedjobs.utils.TestLinkConection;

import org.junit.Test;

import br.eti.kinoshita.testlinkjavaapi.util.TestLinkAPIException;

public class CT8ConsultaDeVagasComPesoDeSalário  extends TestLinkConection{
	
	public CT8ConsultaDeVagasComPesoDeSalário(){
		
		try {
			baseURI = propertiesAssistent.getProperties().get("BASE_URI").toString();
			testProject = propertiesAssistent.getProperties().get("TEST_PROJECT").toString();
			testPlan = propertiesAssistent.getProperties().get("TEST_PLAN").toString();
			build = propertiesAssistent.getProperties().get("BUILD").toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testBuscarPesosRemuneracao() throws TestLinkAPIException {
		testCase="CT8 - Consulta de Vagas - Com Peso de Salário";

		
		try {
			
			given()
		 	.contentType("application/json")
		 .when()
		    .get("/busca/personalizada?latitude=0&longitude=0&salario=9000")
		 .then()
		    .statusCode(200)
		    .body("municipio", hasItem("Brasilia-DF"))
		    .assertThat()
		       .body(matchesJsonSchemaInClasspath("schema_linkedjobs.json"));
			notes="PASSOU - Teste automatizado CT8 - Consulta de Vagas - Com Peso de Salário.(testBuscarPesosRemuneracao)";
			
		} catch (Exception e) {
			notes="FALHOU - Teste automatizado CT8 - Consulta de Vagas - Com Peso de Salário.(testBuscarPesosRemuneracao) \n";
		}finally{
			reportResult(testProject, testPlan, testCase, build, notes, result);
		}
	}
	
}
