package br.edu.ifpb.mpti.linkedjobs.testes;

import static com.jayway.restassured.RestAssured.baseURI;
import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;

import com.linkedjobs.utils.TestLinkConection;

import org.junit.Test;

import br.eti.kinoshita.testlinkjavaapi.util.TestLinkAPIException;

public class ConsultaDetalhamentoFiltroAPI extends TestLinkConection{
	String testProject = "LinkedJobs";
	String testPlan="PT - LinkedJobs v0.5 (Sprint 2)";
	String build="Linkedjobs-v0.5_09062020";
	
	public ConsultaDetalhamentoFiltroAPI(){
		
		baseURI = "http://linkedjobs.duckdns.org/vagas";
	}
	
	@Test
	public void testBuscarFiltroLocalizacao() throws TestLinkAPIException {
		String testCase="CT6 - Consulta de Vagas - Com Filtro de Localização";
		String notes=null;
		String result=null;
		
		try {
			
			given()
		 	.contentType("application/json")
		 .when()
		    .get("/busca/municipio?municipio=Rio")
		 .then()
		    .statusCode(200)
		    .body("municipio", hasItem("Rio de Janeiro-RJ"))
		    .assertThat()
		       .body(matchesJsonSchemaInClasspath("schema_linkedjobs.json"));
			notes="PASSOU - Teste automatizado CT6 - Consulta de Vagas - Com Filtro de Localização.(testBuscarFiltroLocalizacao)";
			
		} catch (Exception e) {
			notes="FALHOU - Teste automatizado CT6 - Consulta de Vagas - Com Filtro de Localização.(testBuscarFiltroLocalizacao)";
		}finally{
			//reportResult(testProject, testPlan, testCase, build, notes, result);
		}
	}
	@Test
	public void testBuscarFiltroRemuneracao() throws TestLinkAPIException {
		String testCase="CT5 - Consulta de Vagas - Com Filtro de Remuneração";
		String notes=null;
		String result=null;
		
		try {
			
			given()
		 	.contentType("application/json")
		 .when()
		    .get("/busca/remuneracao?remuneracao=1000")
		 .then()
		    .statusCode(200)
		    //.body("remuneracao", hasItem(1000.0))
		    .assertThat()
		       .body(matchesJsonSchemaInClasspath("schema_linkedjobs.json"));
			notes="PASSOU - Teste automatizado CT5 - Consulta de Vagas - Com Filtro de Remuneração.(testBuscarFiltroRemuneracao)";
			
		} catch (Exception e) {
			notes="FALHOU - Teste automatizado CT5 - Consulta de Vagas - Com Filtro de Remuneração.(testBuscarFiltroRemuneracao)";
		}finally{
			//reportResult(testProject, testPlan, testCase, build, notes, result);
		}
	}
	@Test
	public void testBuscarPesosGeolocalizacao() throws TestLinkAPIException {
		String testCase="CT7 - Consulta de Vagas - Com Peso de Geolocalização";
		String notes=null;
		String result=null;
		
		try {
			
			given()
		 	.contentType("application/json")
		 .when()
		    .get("/busca/personalizada?"
		    		+ "latitude=-22.8766521181865&longitude=-43.2278751249952"
		    		+ "&salario=0")
		 .then()
		    .statusCode(200)
		    .body("municipio", hasItem("Rio de Janeiro-RJ"))
		    .assertThat()
		       .body(matchesJsonSchemaInClasspath("schema_linkedjobs.json"));
			notes="PASSOU - Teste automatizado CT7 - Consulta de Vagas - Com Peso de Geolocalização.(testInsereBuscaVaga)";
			
		} catch (Exception e) {
			notes="FALHOU - Teste automatizado CT7 - Consulta de Vagas - Com Peso de Geolocalização.(testInsereBuscaVaga) \n";
		}finally{
			//reportResult(testProject, testPlan, testCase, build, notes, result);
		}
	}
	@Test
	public void testBuscarPesosRemuneracao() throws TestLinkAPIException {
		String testCase="CT8 - Consulta de Vagas - Com Peso de Salário";
		String notes=null;
		String result=null;
		
		try {
			
			given()
		 	.contentType("application/json")
		 .when()
		    .get("/busca/personalizada?latitude=0&longitude=0&salario=9000")
		 .then()
		    .statusCode(200)
		    .body("municipio", hasItem("Brasilia-DF"))
		    .assertThat()
		       .body(matchesJsonSchemaInClasspath("schema_linkedjobs.json"));
			notes="PASSOU - Teste automatizado CT8 - Consulta de Vagas - Com Peso de Salário.(testBuscarPesosRemuneracao)";
			
		} catch (Exception e) {
			notes="FALHOU - Teste automatizado CT8 - Consulta de Vagas - Com Peso de Salário.(testBuscarPesosRemuneracao) \n";
		}finally{
			//reportResult(testProject, testPlan, testCase, build, notes, result);
		}
	}
	@Test
	public void testBuscarPesosAplicados() throws TestLinkAPIException {
		String testCase="CT9 - Consulta de Vagas - Com Pesos Aplicados";
		String notes=null;
		String result=null;
		
		try {
			given()
		 	.contentType("application/json")
		 .when()
		    .get("/busca/personalizada?latitude=-22&longitude=-43"
		    		+ "&pesoLocalizacao=1&pesoSalario=0&salario=9000")
		 .then()
		    .statusCode(200)
		    .body("municipio", hasItem("São José dos Campos-SP"))
		    .assertThat()
		       .body(matchesJsonSchemaInClasspath("schema_linkedjobs.json"));
			notes="PASSOU - Teste automatizado CT9 - Consulta de Vagas - Com Pesos Aplicados.(testBuscarPesosAplicados)";
			
		} catch (Exception e) {
			notes="FALHOU - Teste automatizado CT9 - Consulta de Vagas - Com Pesos Aplicados.(testBuscarPesosAplicados) \n";
		}finally{
			//reportResult(testProject, testPlan, testCase, build, notes, result);
		}
	}
	@Test
	public void testBuscarDetalhamentoVaga() throws TestLinkAPIException {
		String testCase="CT10 - Detalhamento da Vagas - Por ID";
		String notes=null;
		String result=null;
		
		try {
			given()
		 	.contentType("application/json")
		 .when()
		    .get("/detalhar?idVaga=5edf97164272428cbf6087c4")
		 .then()
		    .statusCode(200)
		    .body("municipio", containsString("Rio de Janeiro-RJ")).and().body("nome", containsString("Analista de Segurança da Informação"));
		    //.assertThat()
		    //   .body(matchesJsonSchemaInClasspath("schema_linkedjobs.json"));
			notes="PASSOU - Teste automatizado CT10 - Detalhamento da Vagas - Por ID.(testBuscarDetalhamentoVaga)";
			
		} catch (Exception e) {
			notes="FALHOU - Teste automatizado CT10 - Detalhamento da Vagas - Por ID.(testBuscarDetalhamentoVaga) \n";
		}finally{
			//reportResult(testProject, testPlan, testCase, build, notes, result);
		}
	}
	@Test
	public void testObterLinkVaga() throws TestLinkAPIException {
		String testCase="CT11 - Obter Link da Vaga";
		String notes=null;
		String result=null;
		
		try {
			given()
		 	.contentType("application/json")
		 .when()
		    .get("/detalhar?idVaga=5edf97164272428cbf6087c4")
		 .then()
		    .statusCode(200)
		    .body("linkAnuncio", containsString("https://www.catho.com.br/vagas/analista-de-seguranca-da-informacao/16532390/"));
		    //.assertThat()
		       //.body(matchesJsonSchemaInClasspath("schema_linkedjobs.json"));
			notes="PASSOU - Teste automatizado CT11 - Obter Link da Vaga.(testObterLinkVaga)";
			
		} catch (Exception e) {
			notes="FALHOU - Teste automatizado CT11 - Obter Link da Vaga.(testObterLinkVaga) \n";
		}finally{
			//reportResult(testProject, testPlan, testCase, build, notes, result);
		}
	}
}
