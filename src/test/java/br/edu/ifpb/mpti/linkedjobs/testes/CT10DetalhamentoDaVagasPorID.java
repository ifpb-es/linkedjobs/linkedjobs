package br.edu.ifpb.mpti.linkedjobs.testes;

import static com.jayway.restassured.RestAssured.baseURI;
import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;

import com.linkedjobs.utils.TestLinkConection;

import org.junit.Test;

import br.eti.kinoshita.testlinkjavaapi.util.TestLinkAPIException;

public class CT10DetalhamentoDaVagasPorID  extends TestLinkConection{
	String testProject = "LinkedJobs";
	String testPlan="PT - LinkedJobs v0.5 (Sprint 2)";
	String build="Linkedjobs-v0.5_09062020";
	
	public CT10DetalhamentoDaVagasPorID(){
		
		baseURI = "http://linkedjobs.duckdns.org/vagas";
	}
	@Test
	public void testBuscarDetalhamentoVaga() throws TestLinkAPIException {
		String testCase="CT10 - Detalhamento da Vagas - Por ID";
		String notes=null;
				
		try {
			given()
		 	.contentType("application/json")
		 .when()
		    .get("/detalhar?idVaga=5edf97164272428cbf6087c4")
		 .then()
		    .statusCode(200)
		    .body("municipio", containsString("Rio de Janeiro-RJ")).and().body("nome", containsString("Analista de Segurança da Informação"));
		    //.assertThat()
		    //   .body(matchesJsonSchemaInClasspath("schema_linkedjobs.json"));
			notes="PASSOU - Teste automatizado CT10 - Detalhamento da Vagas - Por ID.(testBuscarDetalhamentoVaga)";
			
		} catch (Exception e) {
			notes="FALHOU - Teste automatizado CT10 - Detalhamento da Vagas - Por ID.(testBuscarDetalhamentoVaga) \n";
		}finally{
			reportResult(testProject, testPlan, testCase, build, notes, result);
		}
	}

}
