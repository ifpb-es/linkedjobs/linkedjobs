package br.edu.ifpb.mpti.linkedjobs.testes;

import static com.jayway.restassured.RestAssured.baseURI;
import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.hasItem;

import com.linkedjobs.utils.TestLinkConection;

import org.junit.Test;

import br.eti.kinoshita.testlinkjavaapi.util.TestLinkAPIException;


public class ConsultasAPI extends TestLinkConection{
	String testProject = "LinkedJobs";
	String testPlan="PT - LinkedJobs v0.01 (Sprint 1)";
	String build="Linkedjobs-v0.01_19052020";
	String testPlan2="PT - LinkedJobs v0.5 (Sprint 2)";
	String build2="Linkedjobs-v0.5_09062020";
	
	public ConsultasAPI(){
		
		baseURI = "http://linkedjobs.duckdns.org/vagas";
	}
	
	@Test
	public void testInsereBuscaVaga() throws TestLinkAPIException {
		String testCase="CT2 - Validar Dados Inseridos na Aplicação com Modelo";
		String notes=null;
		String result=null;
		
		
		String body = "{\"descricao\": \"string\",\n" + 
				"  \"faixaRemuneracao\": \"string\",\n" + 
				"  \"latitude\": \"string\",\n" + 
				"  \"linkAnuncio\": \"string\",\n" + 
				"  \"longitude\": \"string\",\n" + 
				"  \"municipio\": \"string\",\n" + 
				"  \"nome\": \"string2\",\n" + 
				"  \"quantidade\": \"string\",\n" + 
				"  \"remuneracao\": 0\n" + 
				"}";
		
		try {
			
			given()
		 	.contentType("application/json")
		 .when()
		 	.body(body)
		    .post("/incluir")
		 .then()
		    .statusCode(201);
			
			given()
		 	.contentType("application/json")
		 .when()
		    .get("/busca?stringDeBusca=string")
		 .then()
		    .statusCode(200)
		    .body("nome", hasItem("string2"))
		    .assertThat()
		       .body(matchesJsonSchemaInClasspath("schema_linkedjobs.json"));
			notes="PASSOU - Teste automatizado CT2 - Validar Dados Inseridos na Aplicação com Modelo.(testInsereBuscaVaga)";
			
		} catch (Exception e) {
			notes="FALHOU - Teste automatizado CT2 - Validar Dados Inseridos na Aplicação com Modelo.(testInsereBuscaVaga)";
		}finally{
			reportResult(testProject, testPlan2, testCase, build2, notes, result);
		}
	}
	
	@Test
	public void testConsultaVagaListagem() throws TestLinkAPIException {
		String testCase="CT3 - Consulta de Vagas - Listagem";
		String notes=null;
		String result=null;
		
		try {
			
			given()
		 	.contentType("application/json")
		 .when()
		    .get("/listar")
		 .then()
		    .statusCode(200)
		    .body("nome", hasItem("string"))
		    .assertThat()
		       .body(matchesJsonSchemaInClasspath("schema_linkedjobs.json"));
			notes="PASSOU - Teste automatizado CT3 - Consulta de Vagas - Listagem.(testConsultaVagaListagem)";
			
		} catch (Exception e) {
			notes="FALHOU - Teste automatizado CT3 - Consulta de Vagas - Listagem.(testConsultaVagaListagem)";
		}finally{
			reportResult(testProject, testPlan, testCase, build, notes, result);
		}
	}
	
	@Test
	public void testConsultaVagaTexto() throws TestLinkAPIException {
		String testCase="CT4 - Consulta de Vagas - Por texto";
		String notes=null;
		String result=null;
		
		try {
			
			given()
		 	.contentType("application/json")
		 .when()
		    .get("/busca?stringDeBusca=string")
		 .then()
		    .statusCode(200)
		    .body("nome", hasItem("string"))
		    .assertThat()
		       .body(matchesJsonSchemaInClasspath("schema_linkedjobs.json"));
			notes="PASSOU - Teste automatizado CT4 - Consulta de Vagas - Texto.(testConsultaVagaTexto)";
			
		} catch (Exception e) {
			notes="FALHOU - Teste automatizado CT4 - Consulta de Vagas - Texto.(testConsultaVagaListagem)";
		}finally{
			reportResult(testProject, testPlan, testCase, build, notes, result);
		}
	}
	

}
