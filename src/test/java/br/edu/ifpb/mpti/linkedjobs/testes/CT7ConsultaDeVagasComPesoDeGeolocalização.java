package br.edu.ifpb.mpti.linkedjobs.testes;

import static com.jayway.restassured.RestAssured.baseURI;
import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.hasItem;

import java.io.IOException;

import com.linkedjobs.utils.TestLinkConection;

import org.junit.Test;

import br.eti.kinoshita.testlinkjavaapi.util.TestLinkAPIException;

public class CT7ConsultaDeVagasComPesoDeGeolocalização  extends TestLinkConection{
	
	public CT7ConsultaDeVagasComPesoDeGeolocalização(){
		try {
			baseURI = propertiesAssistent.getProperties().get("BASE_URI").toString();
			testProject = propertiesAssistent.getProperties().get("TEST_PROJECT").toString();
			testPlan = propertiesAssistent.getProperties().get("TEST_PLAN").toString();
			build = propertiesAssistent.getProperties().get("BUILD").toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	@Test
	public void testBuscarPesosGeolocalizacao() throws TestLinkAPIException {
		testCase="CT7 - Consulta de Vagas - Com Peso de Geolocalização";
		
		try {
			
			given()
		 	.contentType("application/json")
		 .when()
		    .get("/busca/personalizada?"
		    		+ "latitude=-22.8766521181865&longitude=-43.2278751249952"
		    		+ "&salario=0")
		 .then()
		    .statusCode(200)
		    .body("municipio", hasItem("Rio de Janeiro-RJ"))
		    .assertThat()
		       .body(matchesJsonSchemaInClasspath("schema_linkedjobs.json"));
			notes="PASSOU - Teste automatizado CT7 - Consulta de Vagas - Com Peso de Geolocalização.(testInsereBuscaVaga)";
			
		} catch (Exception e) {
			notes="FALHOU - Teste automatizado CT7 - Consulta de Vagas - Com Peso de Geolocalização.(testInsereBuscaVaga) \n";
		}finally{
			reportResult(testProject, testPlan, testCase, build, notes, result);
		}
	}
}
