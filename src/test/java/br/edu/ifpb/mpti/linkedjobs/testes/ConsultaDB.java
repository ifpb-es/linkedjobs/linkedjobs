package br.edu.ifpb.mpti.linkedjobs.testes;
import static com.jayway.restassured.RestAssured.baseURI;

import java.io.IOException;
import java.util.Set;

import com.linkedjobs.utils.TestLinkConection;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import org.junit.Test;

import br.eti.kinoshita.testlinkjavaapi.util.TestLinkAPIException;

public class ConsultaDB extends TestLinkConection{
	String testProject = "LinkedJobs";
	String testPlan="PT - LinkedJobs v0.01 (Sprint 1)";
	String build="Linkedjobs-v0.01_19052020";
	
	public ConsultaDB(){
		
		baseURI = "http://linkedjobs.duckdns.org/vagas";
	}

	@Test
	public void testVerificarConexao() throws IOException {
		MongoClient mongoClient;
		mongoClient = new MongoClient("radarifpb.duckdns.org",27016);
		DB db = mongoClient.getDB("linkedjob");
		org.junit.Assert.assertNotNull(db);
		mongoClient.close();
	}

	@Test
	public void testVerificarColecao() throws IOException {
		MongoClient mongoClient;
		mongoClient = new MongoClient("radarifpb.duckdns.org",27016);
		DB db = mongoClient.getDB("linkedjob");
		org.junit.Assert.assertNotNull(db);

		Set<String> colls = db.getCollectionNames();
		System.out.println(colls.toString());
		org.junit.Assert.assertEquals(1, colls.size());
		org.junit.Assert.assertTrue(colls.contains("Vaga"));

		mongoClient.close();
	}

	@Test
	public void testInserirBuscarItemColecaoBD() throws IOException, TestLinkAPIException {
		String testCase="CT1 - Validar Dados com Modelo no Database";
		String notes=null;
		String result=null;
		
		try {
		
		MongoClient mongoClient;
		mongoClient = new MongoClient("radarifpb.duckdns.org",27016);
		DB db = mongoClient.getDB("linkedjob");
		org.junit.Assert.assertNotNull(db);

		BasicDBObject document = new BasicDBObject("descricao", "teste")
				.append("faixaRemuneracao", "teste")
				.append("latitude", "teste")
				.append("linkAnuncio", "teste")
				.append("longitude", "teste")
				.append("municipio", "teste")
				.append("nome", "teste")
				.append("quantidade", "teste")
				.append("remuneracao", 0);
		
		DBCollection coll = db.getCollection("Vaga");
		
		coll.insert(document);
		

		Set<String> colls = db.getCollectionNames();

		org.junit.Assert.assertEquals(1, colls.size());

		BasicDBObject query = new BasicDBObject("descricao", "teste");

		DBCursor cursor = coll.find(query);
		org.junit.Assert.assertEquals(1, cursor.size());
		DBObject doc = cursor.next();
		org.junit.Assert.assertEquals("teste", doc.get("descricao"));
		coll.remove(document);
		mongoClient.close();
		notes="PASSOU - Teste automatizado CT1 - Validar Dados com Modelo no Database.(testInserirBuscarItemColecaoBD)";
		
		} catch (Exception e) {
			notes="FALHOU - Teste automatizado CT1 - Validar Dados com Modelo no Database.(testInserirBuscarItemColecaoBD)";
		}finally{
			reportResult(testProject, testPlan, testCase, build, notes, result);
		}
	}
}