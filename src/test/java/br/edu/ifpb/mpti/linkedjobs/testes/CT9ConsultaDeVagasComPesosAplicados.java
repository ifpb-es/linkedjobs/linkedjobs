package br.edu.ifpb.mpti.linkedjobs.testes;

import static com.jayway.restassured.RestAssured.baseURI;
import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.hasItem;

import java.io.IOException;

import com.linkedjobs.utils.TestLinkConection;

import org.junit.Test;

import br.eti.kinoshita.testlinkjavaapi.util.TestLinkAPIException;

public class CT9ConsultaDeVagasComPesosAplicados  extends TestLinkConection{
	
	public CT9ConsultaDeVagasComPesosAplicados(){
		
		try {
			baseURI = propertiesAssistent.getProperties().get("BASE_URI").toString();
			testProject = propertiesAssistent.getProperties().get("TEST_PROJECT").toString();
			testPlan = propertiesAssistent.getProperties().get("TEST_PLAN").toString();
			build = propertiesAssistent.getProperties().get("BUILD").toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testBuscarPesosAplicados() throws TestLinkAPIException {
		testCase="CT9 - Consulta de Vagas - Com Pesos Aplicados";
		
		try {
			given()
		 	.contentType("application/json")
		 .when()
		    .get("/busca/personalizada?latitude=-22&longitude=-43"
		    		+ "&pesoLocalizacao=1&pesoSalario=0&salario=9000")
		 .then()
		    .statusCode(200)
		    .body("municipio", hasItem("São José dos Campos-SP"))
		    .assertThat()
		       .body(matchesJsonSchemaInClasspath("schema_linkedjobs.json"));
			notes="PASSOU - Teste automatizado CT9 - Consulta de Vagas - Com Pesos Aplicados.(testBuscarPesosAplicados)";
			
		} catch (Exception e) {
			notes="FALHOU - Teste automatizado CT9 - Consulta de Vagas - Com Pesos Aplicados.(testBuscarPesosAplicados) \n";
		}finally{
			reportResult(testProject, testPlan, testCase, build, notes, result);
		}
	}
	
}
