package br.edu.ifpb.mpti.linkedjobs.testes;

import static com.jayway.restassured.RestAssured.baseURI;
import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.hasItem;

import java.io.IOException;

import com.linkedjobs.utils.TestLinkConection;

import org.junit.Test;

import br.eti.kinoshita.testlinkjavaapi.util.TestLinkAPIException;

public class CT2ValidarDadosInseridosNaAplicaçãoComModelo extends TestLinkConection{
	//String testPlan2="PT - LinkedJobs v0.5 (Sprint 2)";
	//String build2="Linkedjobs-v0.5_09062020";
	
	public CT2ValidarDadosInseridosNaAplicaçãoComModelo(){
		
		try {
			baseURI = propertiesAssistent.getProperties().get("BASE_URI").toString();
			testProject = propertiesAssistent.getProperties().get("TEST_PROJECT").toString();
			testPlan = propertiesAssistent.getProperties().get("TEST_PLAN").toString();
			build = propertiesAssistent.getProperties().get("BUILD").toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testInsereBuscaVaga() throws TestLinkAPIException {
		testCase="CT2 - Validar Dados Inseridos na Aplicação com Modelo";
		
		
		String body = "{\"descricao\": \"string\",\n" + 
				"  \"faixaRemuneracao\": \"string\",\n" + 
				"  \"latitude\": \"string\",\n" + 
				"  \"linkAnuncio\": \"string\",\n" + 
				"  \"longitude\": \"string\",\n" + 
				"  \"municipio\": \"string\",\n" + 
				"  \"nome\": \"string2\",\n" + 
				"  \"quantidade\": \"string\",\n" + 
				"  \"remuneracao\": 0\n" + 
				"}";
		
		try {
			
			given()
		 	.contentType("application/json")
		 .when()
		 	.body(body)
		    .post("/incluir")
		 .then()
		    .statusCode(201);
			
			given()
		 	.contentType("application/json")
		 .when()
		    .get("/busca?stringDeBusca=string")
		 .then()
		    .statusCode(200)
		    .body("nome", hasItem("string2"))
		    .assertThat()
		       .body(matchesJsonSchemaInClasspath("schema_linkedjobs.json"));
			notes="PASSOU - Teste automatizado CT2 - Validar Dados Inseridos na Aplicação com Modelo.(testInsereBuscaVaga)";
			
		} catch (Exception e) {
			notes="FALHOU - Teste automatizado CT2 - Validar Dados Inseridos na Aplicação com Modelo.(testInsereBuscaVaga)";
		}finally{
			reportResult(testProject, testPlan, testCase, build, notes, result);
		}
	}
	
	
}
