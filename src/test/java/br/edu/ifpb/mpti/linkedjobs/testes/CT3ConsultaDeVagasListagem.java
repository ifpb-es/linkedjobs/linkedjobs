package br.edu.ifpb.mpti.linkedjobs.testes;

import static com.jayway.restassured.RestAssured.baseURI;
import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.hasItem;

import java.io.IOException;

import com.linkedjobs.utils.TestLinkConection;

import org.junit.Test;

import br.eti.kinoshita.testlinkjavaapi.util.TestLinkAPIException;

public class CT3ConsultaDeVagasListagem extends TestLinkConection{

	//String testPlan2="PT - LinkedJobs v0.5 (Sprint 2)";
	//String build2="Linkedjobs-v0.5_09062020";
	
	public CT3ConsultaDeVagasListagem(){
		try {
			baseURI = propertiesAssistent.getProperties().get("BASE_URI").toString();
			testProject = propertiesAssistent.getProperties().get("TEST_PROJECT").toString();
			testPlan = propertiesAssistent.getProperties().get("TEST_PLAN").toString();
			build = propertiesAssistent.getProperties().get("BUILD").toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testConsultaVagaListagem() throws TestLinkAPIException {
		testCase="CT3 - Consulta de Vagas - Listagem";
		
		try {
			
			given()
		 	.contentType("application/json")
		 .when()
		    .get("/listar")
		 .then()
		    .statusCode(200)
		    .body("nome", hasItem("string"))
		    .assertThat()
		       .body(matchesJsonSchemaInClasspath("schema_linkedjobs.json"));
			notes="PASSOU - Teste automatizado CT3 - Consulta de Vagas - Listagem.(testConsultaVagaListagem)";
			
		} catch (Exception e) {
			notes="FALHOU - Teste automatizado CT3 - Consulta de Vagas - Listagem.(testConsultaVagaListagem)";
		}finally{
			reportResult(testProject, testPlan, testCase, build, notes, result);
		}
	}
	

}
