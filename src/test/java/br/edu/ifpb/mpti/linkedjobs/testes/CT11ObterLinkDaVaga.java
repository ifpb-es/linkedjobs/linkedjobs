package br.edu.ifpb.mpti.linkedjobs.testes;

import static com.jayway.restassured.RestAssured.baseURI;
import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;

import com.linkedjobs.utils.TestLinkConection;

import org.junit.Test;

import br.eti.kinoshita.testlinkjavaapi.util.TestLinkAPIException;

public class CT11ObterLinkDaVaga  extends TestLinkConection{
	String testProject = "LinkedJobs";
	String testPlan="PT - LinkedJobs v0.5 (Sprint 2)";
	String build="Linkedjobs-v0.5_09062020";
	
	public CT11ObterLinkDaVaga(){
		
		baseURI = "http://linkedjobs.duckdns.org/vagas";
	}
	
	@Test
	public void testObterLinkVaga() throws TestLinkAPIException {
		String testCase="CT11 - Obter Link da Vaga";
		String notes=null;
		
		try {
			given()
		 	.contentType("application/json")
		 .when()
		    .get("/detalhar?idVaga=5edf97164272428cbf6087c4")
		 .then()
		    .statusCode(200)
		    .body("linkAnuncio", containsString("https://www.catho.com.br/vagas/analista-de-seguranca-da-informacao/16532390/"));
		    //.assertThat()
		       //.body(matchesJsonSchemaInClasspath("schema_linkedjobs.json"));

			notes="PASSOU - Teste automatizado CT11 - Obter Link da Vaga.(testObterLinkVaga)";
			
		} catch (Exception e) {
			notes="FALHOU - Teste automatizado CT11 - Obter Link da Vaga.(testObterLinkVaga) \n";
		}finally{
			reportResult(testProject, testPlan, testCase, build, notes, result);
		}
	}
}
