package br.edu.ifpb.mpti.linkedjobs.util;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import br.edu.ifpb.mpti.linkedjobs.model.Vaga;

class CalculardoraRecomendacoesTest {

	@Test
	void testOrdenarVagasPorSalario() {
		String melhorRemuneracao = "5000.0";
		Vaga vagaQuePagaBem = getVaga("Vaga que paga bem", melhorRemuneracao, null, null);
		Vaga vagaQuePagaMal = getVaga("Vaga que paga mal", "1000.0", null, null);
		
		List<Vaga> vagasDisponiveis = new ArrayList<Vaga>();
		vagasDisponiveis.add(vagaQuePagaBem);
		vagasDisponiveis.add(vagaQuePagaMal);
		
		List<Vaga> vagasOrdenadasPorSimilaridade = new CalculardoraRecomendacoes().ordenarVagasPorSimilaridadeComPesos(vagasDisponiveis , -44.4436134229207, -22.4711835861228, 5000.0, 3, 2);
		assertEquals(vagasOrdenadasPorSimilaridade.get(0).getRemuneracao(),melhorRemuneracao);
	}

	@Test
	void testOrdenarVagasPorSalarioComListaInvertida() {
		String melhorRemuneracao = "5000.0";
		Vaga vagaQuePagaBem = getVaga("Vaga que paga bem", melhorRemuneracao, null, null);
		Vaga vagaQuePagaMal = getVaga("Vaga que paga mal", "1000.0", null, null);
		
		List<Vaga> vagasDisponiveisInvertidas = new ArrayList<Vaga>();
		vagasDisponiveisInvertidas.add(vagaQuePagaMal);
		vagasDisponiveisInvertidas.add(vagaQuePagaBem);
				
		List<Vaga> vagasOrdenadasPorSimilaridade2 = new CalculardoraRecomendacoes().ordenarVagasPorSimilaridadeComPesos(vagasDisponiveisInvertidas , -44.4436134229207, -22.4711835861228, 5000.0, 3, 2);
		assertEquals(vagasOrdenadasPorSimilaridade2.get(0).getRemuneracao(),melhorRemuneracao);
	}

	@Test
	void testOrdenarVagasPorLocalizacao() {
		Vaga vagaMaisProxima = getVaga("Vaga mais próxima", "5000.0", "-44.4436134229207", "-22.4711835861228");
		Vaga vagaMaisDistante = getVaga("Vaga distante", "5000.0", "-44.4436134286528", "-22.4711835898132");
		
		List<Vaga> vagasDisponiveis = new ArrayList<Vaga>();
		vagasDisponiveis.add(vagaMaisProxima);
		vagasDisponiveis.add(vagaMaisDistante);
		
		List<Vaga> vagasOrdenadasPorSimilaridade = new CalculardoraRecomendacoes().ordenarVagasPorSimilaridadeComPesos(vagasDisponiveis , -44.4436134229207, -22.4711835861228, 5000.0, 3, 2);
		assertEquals(vagasOrdenadasPorSimilaridade.get(0).getNome(),"Vaga mais próxima");
	}

	@Test
	void testOrdenarVagasPorLocalizacaoInvertida() {
		Vaga vagaMaisProxima = getVaga("Vaga mais próxima", "5000.0", "-44.4436134229207", "-22.4711835861228");
		Vaga vagaMaisDistante = getVaga("Vaga distante", "5000.0", "-44.4436134286528", "-22.4711835898132");
		
		List<Vaga> vagasDisponiveis = new ArrayList<Vaga>();
		vagasDisponiveis.add(vagaMaisDistante);
		vagasDisponiveis.add(vagaMaisProxima);
		
		List<Vaga> vagasOrdenadasPorSimilaridade = new CalculardoraRecomendacoes().ordenarVagasPorSimilaridadeComPesos(vagasDisponiveis , -44.4436134229207, -22.4711835861228, 5000.0, 3, 2);
		assertEquals(vagasOrdenadasPorSimilaridade.get(0).getNome(),"Vaga mais próxima");
	}

	@Test
	void testOrdenarVagasPorLocalizacaoESalario() {
		Vaga vagaMaisProxima = getVaga("Vaga mais próxima", "5000.0", "-44.4436134229207", "-22.4711835861228");
		Vaga vagaMaisInteressante = getVaga("Vaga top", "8000.0", "-44.4436134229207", "-22.4711835861228");
		
		List<Vaga> vagasDisponiveis = new ArrayList<Vaga>();
		vagasDisponiveis.add(vagaMaisProxima);
		vagasDisponiveis.add(vagaMaisInteressante);
		
		List<Vaga> vagasOrdenadasPorSimilaridade = new CalculardoraRecomendacoes().ordenarVagasPorSimilaridadeComPesos(vagasDisponiveis , -44.4436134229207, -22.4711835861228, 6000.0, 3, 2);
		assertEquals(vagasOrdenadasPorSimilaridade.get(0).getNome(),"Vaga top");
	}

	@Test
	void testOrdenarVagasPorLocalizacaoESalarioComPesoMaiorLocalizacao() {
		Vaga vagaMaisProxima = getVaga("Vaga mais próxima", "5000.0", "-44.4436134229207", "-22.4711835861228");
		Vaga vagaMaisInteressante = getVaga("Vaga que paga mais", "8000.0", "-45.4436134299999", "-19.4711835899999");
		
		List<Vaga> vagasDisponiveis = new ArrayList<Vaga>();
		vagasDisponiveis.add(vagaMaisProxima);
		vagasDisponiveis.add(vagaMaisInteressante);
		
		List<Vaga> vagasOrdenadasPorSimilaridade = new CalculardoraRecomendacoes().ordenarVagasPorSimilaridadeComPesos(vagasDisponiveis , -44.4436134229207, -22.4711835861228, 6000.0, 3, 10);
		assertEquals(vagasOrdenadasPorSimilaridade.get(0).getNome(),"Vaga mais próxima");
	}
	
	@Test
	void testOrdenarVagasPorLocalizacaoESalarioComPesoMaiorSalario() {
		Vaga vagaMaisProxima = getVaga("Vaga mais próxima", "5000.0", "-44.4436134229207", "-22.4711835861228");
		Vaga vagaMaisInteressante = getVaga("Vaga que paga mais", "8000.0", "-45.4436134299999", "-19.4711835899999");
		
		List<Vaga> vagasDisponiveis = new ArrayList<Vaga>();
		vagasDisponiveis.add(vagaMaisProxima);
		vagasDisponiveis.add(vagaMaisInteressante);
		
		List<Vaga> vagasOrdenadasPorSimilaridade = new CalculardoraRecomendacoes().ordenarVagasPorSimilaridadeComPesos(vagasDisponiveis , -44.4436134229207, -22.4711835861228, 6000.0, 10, 5);
		assertEquals(vagasOrdenadasPorSimilaridade.get(0).getNome(),"Vaga que paga mais");
	}
	
	private Vaga getVaga(String nomeVaga, String remuneracao, String latitude, String longitude) {
		Vaga vaga = new Vaga();
		vaga.setNome(nomeVaga);
		vaga.setRemuneracao(Double.parseDouble(remuneracao));
		vaga.setLatitude(latitude);
		vaga.setLongitude(longitude);
		return vaga;
	}

}
