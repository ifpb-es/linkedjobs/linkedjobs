package br.edu.ifpb.mpti.linkedjobs.testes;

import static com.jayway.restassured.RestAssured.baseURI;
import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

import java.io.IOException;

import com.linkedjobs.utils.TestLinkConection;

import org.junit.Test;

import br.eti.kinoshita.testlinkjavaapi.util.TestLinkAPIException;

public class CT5ConsultaDeVagasComFiltroDeRemuneração extends TestLinkConection{
	String testProject = "LinkedJobs";
	String testPlan="PT - LinkedJobs v0.5 (Sprint 2)";
	String build="Linkedjobs-v0.5_09062020";
	
	public CT5ConsultaDeVagasComFiltroDeRemuneração(){
		
		try {
			baseURI = propertiesAssistent.getProperties().get("BASE_URI").toString();
			testProject = propertiesAssistent.getProperties().get("TEST_PROJECT").toString();
			testPlan = propertiesAssistent.getProperties().get("TEST_PLAN").toString();
			build = propertiesAssistent.getProperties().get("BUILD").toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testBuscarFiltroRemuneracao() throws TestLinkAPIException {
		testCase="CT5 - Consulta de Vagas - Com Filtro de Remuneração";
		
		try {
			
			given()
		 	.contentType("application/json")
		 .when()
		    .get("/busca/remuneracao?remuneracao=1000")
		 .then()
		    .statusCode(200)
		    //.body("remuneracao", hasItem(1000.0))
		    .assertThat()
		       .body(matchesJsonSchemaInClasspath("schema_linkedjobs.json"));
			notes="PASSOU - Teste automatizado CT5 - Consulta de Vagas - Com Filtro de Remuneração.(testBuscarFiltroRemuneracao)";
			
		} catch (Exception e) {
			notes="FALHOU - Teste automatizado CT5 - Consulta de Vagas - Com Filtro de Remuneração.(testBuscarFiltroRemuneracao)";
		}finally{
			reportResult(testProject, testPlan, testCase, build, notes, result);
		}
	}
	
}
