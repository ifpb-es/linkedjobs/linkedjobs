package br.edu.ifpb.mpti.linkedjobs.services;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.bson.types.ObjectId;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import br.edu.ifpb.mpti.linkedjobs.model.Vaga;
import br.edu.ifpb.mpti.linkedjobs.services.dto.VagaMapper;
import br.edu.ifpb.mpti.linkedjobs.services.dto.VagaSimplificadaDTO;
import br.edu.ifpb.mpti.linkedjobs.util.CalculardoraRecomendacoes;

@Path("/vagas")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class VagaService {

    @Inject
    VagaMapper vagaMapper;

    @GET
    @Tag(name = "vaga")
    @Path("/listar")
    @Operation(description = "Serviço de listagem de todas as vagas armazenadas no banco de dados em formato JSON", summary = "Lista todas as vagas armazenadas no banco")
    public List<VagaSimplificadaDTO> listarVagas() {
        return vagaMapper.toListaDeVagasSimplificadas(Vaga.listAll());
    }

    @GET
    @Tag(name = "vaga")
    @Path("/detalhar")
    @Operation(description = "Serviço de buscar uma vaga para obter todos os seus detalhes", summary = "Detalhar uma vaga")
    public Vaga detalharVaga(@QueryParam("idVaga") String idVaga) {
        return Vaga.findById(new ObjectId(idVaga));
    }

    @GET
    @Tag(name = "vaga")
    @Path("/busca")
    @Operation(description = "Serviço de busca das vagas que possuírem a palavra ou frase exata em sua descrição", summary = "Lista de vagas que possuem a palavra buscada em sua descrição")
    public List<VagaSimplificadaDTO> localizarVaga(@QueryParam("stringDeBusca") String stringDeBusca) {
        if(stringDeBusca == null){
            return null;
        }    
        return vagaMapper.toListaDeVagasSimplificadas(Vaga.localizarVagaPorDescricao(stringDeBusca));
    }

    @GET
    @Tag(name = "vaga")
    @Path("/busca/municipio")
    @Operation(description = "Serviço de busca das vagas que estiverem no município buscado", summary = "Lista de vagas do município buscado")
    public List<VagaSimplificadaDTO> localizarVagaPorMunicipio(@QueryParam("municipio") String stringDeBusca) {
        if(stringDeBusca == null){
            return null;
        }    
        return vagaMapper.toListaDeVagasSimplificadas(Vaga.localizarVagaPorMunicipio(stringDeBusca));
    }

    @GET
    @Tag(name = "vaga")
    @Path("/busca/remuneracao")
    @Operation(description = "Serviço de busca das vagas que possuírem remuneração igual ou maior que a pretendida", summary = "Lista de vagas com remuneração igual ou maior que a pretendida")
    public List<VagaSimplificadaDTO> localizarVagaPorRemuneracao(@QueryParam("remuneracao") Double remuneracao) {
        if(remuneracao == null){
            return null;
        }    
        return vagaMapper.toListaDeVagasSimplificadas(Vaga.localizarVagaPorRemuneracao(remuneracao));
    }

    @GET
    @Tag(name = "vaga")
    @Path("/busca/personalizada")
    @Operation(description = "Serviço de listagem das vagas com maior similaridade com o candidato.", summary = "Lista de vagas que possuem mais similiridade com o candidato, a partir do salario pretendido e da localizaçao geográfica")
    public List<Vaga> localizarVagaPersonalizada(@QueryParam("salario") String salarioPretendido, @QueryParam("latitude") String latitude, 
    		@QueryParam("longitude") String longitude, @QueryParam("pesoLocalizacao") Integer pesoLocalizacao, @QueryParam("pesoSalario") Integer pesoSalario) {
        if(salarioPretendido == null || latitude == null || longitude == null){
            
        }
        List<Vaga> vagas = Vaga.listAll();
		if (pesoLocalizacao == null || pesoSalario == null) {
        	return new CalculardoraRecomendacoes().ordenarVagasPorSimilaridade(vagas, Double.parseDouble(latitude), Double.parseDouble(longitude), Double.parseDouble(salarioPretendido));
        }
        return new CalculardoraRecomendacoes().ordenarVagasPorSimilaridadeComPesos(vagas, Double.parseDouble(latitude), Double.parseDouble(longitude), Double.parseDouble(salarioPretendido), pesoLocalizacao.intValue(), pesoSalario.intValue());
    }

    @POST
    @Tag(name = "vaga")
    @Path("/incluir")
    @Transactional
    @Operation(description = "Serviço de inclusão de alguma vaga para fins de teste. É importante observar a estrutura do JSON utilizado", summary = "Inclui uma vaga para fins de teste")
    public Response incluirVaga(Vaga vagaParametro){
        Vaga vagaPersistida = new Vaga();
        vagaPersistida.setDescricao(vagaParametro.getDescricao());
        vagaPersistida.setLinkAnuncio(vagaParametro.getLinkAnuncio());
        vagaPersistida.setMunicipio(vagaParametro.getMunicipio());
        vagaPersistida.setNome(vagaParametro.getNome());
        vagaPersistida.setRemuneracao(vagaParametro.getRemuneracao());
        vagaPersistida.setQuantidade(vagaParametro.getQuantidade());
        
        vagaPersistida.persist();

        return Response.status(Status.CREATED).build();
    }
    
}