package br.edu.ifpb.mpti.linkedjobs.model;

import java.util.List;
import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;

@MongoEntity(collection = "empresas")
public class Empresa extends PanacheMongoEntity{

	private String cnpj;
	
	private String nome;

	private String nicho;

	private String cidade;

	private String estado;

	private String cep;

	private String logradouro;

	private String tipoLogradouro;

	private Integer numero;

	private String complemento;

    private List<Candidato> vagas;

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(final String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(final String nome) {
		this.nome = nome;
	}

	public String getNicho() {
		return nicho;
	}

	public void setNicho(final String nicho) {
		this.nicho = nicho;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(final String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(final String estado) {
		this.estado = estado;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(final String cep) {
		this.cep = cep;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(final String logradouro) {
		this.logradouro = logradouro;
	}

	public String getTipoLogradouro() {
		return tipoLogradouro;
	}

	public void setTipoLogradouro(final String tipoLogradouro) {
		this.tipoLogradouro = tipoLogradouro;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(final Integer numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(final String complemento) {
		this.complemento = complemento;
	}

	public List<Candidato> getJobs() {
		return vagas;
	}

	public void setJobs(final List<Candidato> jobs) {
		this.vagas = jobs;
	}

}