package br.edu.ifpb.mpti.linkedjobs.services.dto;

import java.util.List;

import org.mapstruct.Mapper;

import br.edu.ifpb.mpti.linkedjobs.model.Vaga;

@Mapper(componentModel = "cdi")
public interface VagaMapper {
    
    public Vaga toVaga(VagaSimplificadaDTO vagaSimplificada);

    public VagaSimplificadaDTO toVagaSimplificada(Vaga vaga);

    public List<VagaSimplificadaDTO> toListaDeVagasSimplificadas(List<Vaga> vagas);

}