package br.edu.ifpb.mpti.linkedjobs.model;

import java.util.List;

import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;

@MongoEntity(collection = "candidatos")
public class Candidato extends PanacheMongoEntity {

    private Empresa empresa;

    private Integer remuneracao;

    private String nome;

    private Integer quantidade;

    private Integer cargaHoraria;

    private String linkAnuncio;

    private List<Competencia> competencias;

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Integer getRemuneracao() {
		return remuneracao;
	}

	public void setRemuneracao(Integer remuneracao) {
		this.remuneracao = remuneracao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Integer getCargaHoraria() {
		return cargaHoraria;
	}

	public void setCargaHoraria(Integer cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}

	public String getLinkAnuncio() {
		return linkAnuncio;
	}

	public void setLinkAnuncio(String linkAnuncio) {
		this.linkAnuncio = linkAnuncio;
	}

	public List<Competencia> getCompetencias() {
		return competencias;
	}

	public void setCompetencias(List<Competencia> competencias) {
		this.competencias = competencias;
	}

	
    
}