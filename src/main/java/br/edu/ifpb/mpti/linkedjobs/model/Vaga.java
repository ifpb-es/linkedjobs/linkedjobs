package br.edu.ifpb.mpti.linkedjobs.model;

import java.util.List;

import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;

@MongoEntity(collection = "Vaga")
public class Vaga extends PanacheMongoEntity {

	private String faixaRemuneracao;

	private Double remuneracao;

	private String nome;

	private String descricao;

	private String municipio;

	private String quantidade;

	private String linkAnuncio;

	private String latitude;

	private String longitude;

	public Double getRemuneracao() {
		return remuneracao;
	}

	public void setRemuneracao(final Double remuneracao) {
		this.remuneracao = remuneracao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(final String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(final String descricao) {
		this.descricao = descricao;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(final String municipio) {
		this.municipio = municipio;
	}

	public String getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(final String quantidade) {
		this.quantidade = quantidade;
	}

	public String getLinkAnuncio() {
		return linkAnuncio;
	}

	public void setLinkAnuncio(final String linkAnuncio) {
		this.linkAnuncio = linkAnuncio;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public static List<Vaga> localizarVagaPorDescricao(final String stringDeBusca){
		return list("descricao like ?1", stringDeBusca);
	}

	public static List<Vaga> localizarVagaPorMunicipio(final String municipio){
		return list("municipio like ?1", municipio);
	}

	public static List<Vaga> localizarVagaPorRemuneracao(final Double remuneracao){
		return list("remuneracao >= ?1", remuneracao);
	}

	public String getFaixaRemuneracao() {
		return faixaRemuneracao;
	}

	public void setFaixaRemuneracao(String faixaRemuneracao) {
		this.faixaRemuneracao = faixaRemuneracao;
	}

}