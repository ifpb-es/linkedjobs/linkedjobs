package br.edu.ifpb.mpti.linkedjobs.services.application;

import javax.ws.rs.core.Application;

import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Info;

@OpenAPIDefinition(info = @Info(description = "Conjunto de serviços para intermediação entre vagas e candidatos", title = "Linked Jobs - em busca do emprego ideal" , version = "1.0"))
public class LinkedJobs extends Application {
    
}