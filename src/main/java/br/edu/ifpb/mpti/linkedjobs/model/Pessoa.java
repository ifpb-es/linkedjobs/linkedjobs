package br.edu.ifpb.mpti.linkedjobs.model;

import java.util.Date;

import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;

@MongoEntity(collection = "pessoas")
public class Pessoa extends PanacheMongoEntity{

    private String cpf;

    private String name;

    private Date dataDeNascimento;

    private String sexo;

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDataDeNascimento() {
		return this.dataDeNascimento;
	}

	public void setDataDeNascimento(Date datadeNascimento) {
		this.dataDeNascimento = datadeNascimento;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

    

    

}