package br.edu.ifpb.mpti.linkedjobs.util;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.edu.ifpb.mpti.linkedjobs.model.Vaga;

public class CalculardoraRecomendacoes {
	
	public List<Vaga> ordenarVagasPorSimilaridadeComPesos(List<Vaga> vagasDisponiveis, final double latitudeCandidato, final double longitudeCandidato, final double salarioCandidato, final int pesoSalario, final int pesoLocalizacao) {
		Collections.sort(vagasDisponiveis, new Comparator<Vaga>() {  
            public int compare(Vaga vaga1, Vaga vaga2) {
            	Double scoreVaga1 = getScoreVaga(vaga1, latitudeCandidato, longitudeCandidato, salarioCandidato, pesoLocalizacao, pesoSalario);
            	Double scoreVaga2 = getScoreVaga(vaga2, latitudeCandidato, longitudeCandidato, salarioCandidato, pesoLocalizacao, pesoSalario);
            	
            	return scoreVaga2.compareTo(scoreVaga1);  
            }

			private Double getScoreVaga(Vaga vaga, double latitudeCandidato, double longitudeCandidato, double salarioCandidato,
					int pesoLocalizacao, int pesoSalario) {
				double fatorLocalizacao = 1;
				double fatorSalario = 1;
				if(vaga.getLatitude() != null && !vaga.getLatitude().isEmpty()
						&& vaga.getLongitude() != null && !vaga.getLongitude().isEmpty()) {
	            	 fatorLocalizacao = 100 * Math.sqrt(Math.pow(Double.parseDouble(vaga.getLatitude()) - latitudeCandidato,2) +  Math.pow(Double.parseDouble(vaga.getLongitude()) - longitudeCandidato,2)) * pesoLocalizacao;
            	}
            	if(vaga.getRemuneracao() != null && !(vaga.getRemuneracao() > 0.0)) {
            		fatorSalario = (salarioCandidato - vaga.getRemuneracao()) * pesoSalario;
            		fatorSalario = fatorSalario < 0 ? 0.0 : fatorSalario;
            	}            	
            	
				return 1 / Math.sqrt( Math.pow(fatorLocalizacao,2) + Math.pow(fatorSalario,2));
			}  

		}); 
		return vagasDisponiveis;
	}

	public List<Vaga> ordenarVagasPorSimilaridade(List<Vaga> vagasDisponiveis, final double latitudeCandidato, final double longitudeCandidato, final double salarioCandidato) {
		return this.ordenarVagasPorSimilaridadeComPesos(vagasDisponiveis, latitudeCandidato, longitudeCandidato, salarioCandidato, 1, 1);
	}

}
