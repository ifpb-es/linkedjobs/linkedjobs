package br.edu.ifpb.mpti.linkedjobs.services.dto;

import org.bson.types.ObjectId;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

public class VagaSimplificadaDTO {
	
	@Schema(description = "Identificador da vaga no sistema")
	private ObjectId id;
	
	@Schema(description = "Remuneração ofertada para a Vaga", minimum = "0")
	private Double remuneracao;

	@Schema(description = "Nome de identificação da vaga")
	private String nome;

	@Schema(description = "Município de localização da vaga")
	private String municipio;

	public Double getRemuneracao() {
		return remuneracao;
	}

	public void setRemuneracao(Double remuneracao) {
		this.remuneracao = remuneracao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}



}