import time
import csv
from selenium import webdriver

class extracao_linkedin:

    def __init__(self, nome_bot):
        self.driver = webdriver.Chrome(executable_path='./chromedriver/chromedriver')

    def abrirPagina(self, pagina):
        print("pagina: " + str(pagina))
        self.driver.get('https://www.linkedin.com/jobs/search/?geoId=106057199&keywords=tecnologia%20da%20informa%C3%A7%C3%A3o&location=Brasil&start=' + str(pagina*25))
        time.sleep(4)        

    def abrirMaisVagas(self):
        section = self.driver.find_element_by_class_name("results__list")
        button = section.find_elements_by_tag_name("button")[0]
        button.click()
        time.sleep(5)
                       

    def iniciarExtracao(self):

        self.abrirPagina(0)
        
        # Execute Javascript code on webpage
        jobs_ul = self.driver.find_element_by_class_name("jobs-search__results-list")
        jobs_lis = jobs_ul.find_elements_by_tag_name("li")
        print(len(jobs_lis))

        #for list in jobs_lis :
        #    self.driver.execute_script("arguments[0].scrollIntoView();", list )
        #    time.sleep(2)      
        continuarProcessando = "true"    
        vagaAtual = 0
        with open('linkedin.csv', mode='w', encoding="utf-8") as csv_file:
            csv_file.write("Vagas_remuneracao;Vagas_nome;Vagas_descricao;Vagas_localizacao;Vagas_empresa;Vagas_link_anuncio;Latitude;Longitude")
        
            while continuarProcessando:
                vaga_li = jobs_lis[vagaAtual]
                self.driver.execute_script("arguments[0].scrollIntoView();", vaga_li )
                vaga_a = vaga_li.find_elements_by_tag_name("a")[0]
                time.sleep(4)    
                vaga_a.click()
                time.sleep(4)    
                vagaAtual += 1
                print(vagaAtual)
                titulo = self.driver.find_element_by_class_name("topcard__title").text
                print(titulo)
                div_cabecalho = self.driver.find_element_by_class_name("topcard__content-left")
                a_link = div_cabecalho.find_elements_by_tag_name("a")
                h3_cabecalho = div_cabecalho.find_elements_by_tag_name("h3")
                h3_cidade = h3_cabecalho[0]
                link = a_link[0].get_attribute("href")    
                #print(link)    
                empresa = h3_cidade.find_elements_by_tag_name("span")[0].text 
                print(empresa)
                cidade = h3_cidade.find_elements_by_tag_name("span")[1].text
                print(cidade)
                if len(h3_cabecalho) > 2 :
                    salario = h3_cabecalho[1].find_elements_by_tag_name("div")[0].text 
                else :
                    salario = "A Combinar"
                print(salario)
                section_descricao = self.driver.find_element_by_class_name("description")
                div_descricao = section_descricao.find_elements_by_tag_name("div")[0]
                sections_div_descricao = div_descricao.find_elements_by_tag_name("section")
                #if (len(sections_div_descricao) > 0):
                #    button_exibir_mais = sections_div_descricao[0].find_elements_by_tag_name("button")[0]
                #    button_exibir_mais.click()
                #    time.sleep(5)
                descricao = div_descricao.text.replace('\n', ' ').replace('\r', '')
                #print(descricao)
                #quantidadevagas
                latitude = ""
                longitude = ""
                municipio = cidade[:-4]
                print(municipio)
                with open('cidades.csv', mode='r', encoding="utf-8") as csv_cidades:    
                    cidades = csv.reader(csv_cidades, delimiter=";")    
                    for row in cidades:
                        if row[1].lower() == municipio.lower() :
                            latitude = row[3]
                            longitude = row[2]  
                            break                           
                print("latitude: " + latitude)
                print("longitude: " + longitude)
                csv_file.write('\n')
                csv_file.write("\"" + salario + "\";\"" + titulo + "\";\"" + descricao + "\";\"" + cidade + "\";\"" + empresa + "\";\"" + link + "\";\"" + latitude + "\";\"" + longitude + "\"")
                jobs_lis = jobs_ul.find_elements_by_tag_name("li")
                if  len(jobs_lis) == vagaAtual :
                    self.abrirMaisVagas()
                    jobs_lis = jobs_ul.find_elements_by_tag_name("li")
                continuarProcessando = len(jobs_lis) > vagaAtual
   