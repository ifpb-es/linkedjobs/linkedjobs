import time

from selenium import webdriver

class extracao_catho:

    def __init__(self, nome_bot):
        self.driver = webdriver.Chrome(executable_path='./chromedriver/chromedriver')

    def abrirPagina(self, pagina):
        print("pagina: " + str(pagina))
        self.driver.get('https://www.catho.com.br/vagas/analista-de-tecnologia-da-informacao/?perfil_id=1&q=Analista+de+Tecnologia+da+Informacao&pais_id=31&where_search=1&how_search=2&faixa_sal_id_combinar=1&order=score&page=' + str(pagina))
        time.sleep(4)        

    def iniciarExtracao(self):

        self.abrirPagina(1)
        
        section_result = self.driver.find_element_by_id("search-result")
        # Execute Javascript code on webpage
        paginacao = section_result.find_elements_by_tag_name("nav")[0]
        paginas = int(paginacao.find_elements_by_tag_name("a")[5].text)

        print(paginas)

        with open('catho.csv', mode='w', encoding="utf-8") as csv_file:
            csv_file.write("Vagas_remuneracao;Vagas_nome;Vagas_descricao;Vagas_localizacao;Vagas_quantidade;Vagas_link_anuncio;Latitude;Longitude")

            vagas_emprego = []

            for paginaAtual in range(1, paginas+1):
                section_result = self.driver.find_element_by_id("search-result")
                html_list = section_result.find_elements_by_tag_name("ul")[0]
                itens = html_list.find_elements_by_tag_name("li")
                count = 1
                for item in itens:
                    print("item: " + str(count) + ", id: " + item.get_attribute("id"))
                    count+=1
                    section = item.find_elements_by_tag_name("section")
                    if (len(section) > 0):
                        div = section[0].find_elements_by_tag_name("div")
                        header = div[0].find_elements_by_tag_name("header")
                        article = div[0].find_elements_by_tag_name("article")
                        div_article = article[0].find_elements_by_tag_name("div")
                        descricao = div_article[0].find_elements_by_tag_name("div")
                        print(descricao)
                        h2 = header[0].find_elements_by_tag_name("h2")
                        a = h2[0].find_elements_by_tag_name("a")
                        div2 = h2[0].find_elements_by_tag_name("div")
                        salario1 = div2[0].find_element_by_class_name("salarioLocal")
                        cidades = div2[0].find_element_by_class_name("cidades")
                        span = cidades.find_elements_by_tag_name("span")

                        #titulo = item.get_attribute("data-gtm-title")
                        titulo = a[0].text
                        quantidadevagas = item.get_attribute("data-quantidadevagas")        
                        descricao = div_article[0].find_elements_by_tag_name("div")[0].get_attribute("fulltext").replace('\n', ' ').replace('\r', '')
                        cidade = span[0].find_elements_by_tag_name("a")[0].text
                        salario = div2[0].find_element_by_class_name("salarioLocal").text
                        link = a[0].get_attribute("href").split('?')[0]
                        municipio = cidade[:-3]
                        print(municipio)
                        with open('cidades.csv', mode='r', encoding="utf-8") as csv_cidades:    
                            cidades = csv.reader(csv_cidades, delimiter=";")    
                            for row in cidades:
                                if row[1].lower() == municipio.lower() :
                                    latitude = row[3]
                                    longitude = row[2]  
                                    break    
                        csv_file.write('\n')
                        csv_file.write("\"" + salario + "\";\"" + titulo + "\";\"" + descricao + "\";\"" + cidade + "\";\"" + quantidadevagas + "\";\"" + link + "\";\"" + latitude + "\";\"" + longitude + "\"")
                self.abrirPagina(paginaAtual+1)                