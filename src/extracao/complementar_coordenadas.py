import csv

class complementar_coordenadas:

    def __init__(self):
        print("Começando")
       
    def complementarCoordenadasCatho(self):

        with open('catho_v2.csv', mode='w', encoding="utf-8") as csv_file_coordenadas:
            csv_file_coordenadas.write("Vagas_remuneracao;Vagas_nome;Vagas_descricao;Vagas_localizacao;Vagas_quantidade;Vagas_link_anuncio;Latitude;Longitude")

            with open('catho.csv', mode='r', encoding="utf-8") as csv_file:
                vagas = csv.reader(csv_file, delimiter=";")
                cabecalho = True
                for vaga in vagas:
                    if not cabecalho :
                        latitude = ""
                        longitude = ""    
                        with open('cidades.csv', mode='r', encoding="utf-8") as csv_cidades:    
                            cidades = csv.reader(csv_cidades, delimiter=";")    
                            for cidade in cidades:
                                if cidade[1].lower() == vaga[3][:-3].lower() :
                                    latitude = cidade[3]
                                    longitude = cidade[2]  
                                    break
                        csv_file_coordenadas.write('\n')
                        csv_file_coordenadas.write("\"" + vaga[0] + "\";\"" + vaga[1] + "\";\"" + vaga[2] + "\";\"" + vaga[3] + "\";\"" + vaga[4] + "\";\"" + vaga[5] + "\";\"" + latitude + "\";\"" + longitude + "\"")                           
                    cabecalho = False
    
    def complementarCoordenadasLinkedin(self):

        with open('linkedin_v2.csv', mode='w', encoding="utf-8") as csv_file_coordenadas:
            csv_file_coordenadas.write("Vagas_remuneracao;Vagas_nome;Vagas_descricao;Vagas_localizacao;Vagas_empresa;Vagas_link_anuncio;Latitude;Longitude")
        
            with open('linkedin.csv', mode='r', encoding="utf-8") as csv_file:
                vagas = csv.reader(csv_file, delimiter=";")
                cabecalho = True
                for vaga in vagas:
                    if not cabecalho :
                        latitude = ""
                        longitude = ""    
                        with open('cidades.csv', mode='r', encoding="utf-8") as csv_cidades:    
                            cidades = csv.reader(csv_cidades, delimiter=";")    
                            for cidade in cidades:
                                if cidade[1].lower() == vaga[3][:-4].lower() :
                                    latitude = cidade[3]
                                    longitude = cidade[2]  
                                    break
                        csv_file_coordenadas.write('\n')
                        csv_file_coordenadas.write("\"" + vaga[0] + "\";\"" + vaga[1] + "\";\"" + vaga[2] + "\";\"" + vaga[3] + "\";\"" + vaga[4] + "\";\"" + vaga[5] + "\";\"" + latitude + "\";\"" + longitude + "\"")                           
                    cabecalho = False