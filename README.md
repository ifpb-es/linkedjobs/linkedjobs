## Executando a aplicação em modo desenvolvimento

Executar a linha de comando na raiz do projeto
```
./mvnw quarkus:dev
```

## Pré-requisitos

Java 13
Maven 3.6.3